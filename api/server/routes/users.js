import express from 'express';
import User from '../../models/user';
import mongoose from 'mongoose';

const router = express.Router();

router.get('/', (req, res) => {
    const users = User.find((err, users) => res.json(users));
});

export default router;
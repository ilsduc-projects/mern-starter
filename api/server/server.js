import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';

import usersRouter from './routes/users';

let uri = "mongodb://root:password@db:27017/api?authSource=admin";
mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology:true });

const app = express();

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/**
 * Users routes
 */
app.use('/users', usersRouter);

app.listen(process.env.PORT || 8080);

export default app;

#!/usr/bin/env bash

docker-compose build
docker-compose up -d

# install express app and others tools
docker-compose exec -it api npm i --save express mongoose babel-cli babel-preset-env && npm i
docker-compose exec -it client npm i

docker-compose down